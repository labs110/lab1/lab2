import math

# Блок ввода данных
try:
    a = float(input('Введите  переменную а: '))
    x = float(input('Введите переменную х: '))
    x1 = int(input('Введите число границ x от: '))
    x2 = int(input('Введите число границ x до: '))
    value = int(input('Выберите значение, где G = 1, F = 2, Y = 3 : '))
    step = int(input('Выберите, сколько шагов будет для вычисления: '))
except ValueError:
    print('Ошибка введённых данных')
    exit(1)

# Блок расчёта и вывода данных G
for x1 in x2
if value == 1:
    try:
        G = (6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)) / (9 * a ** 2 + 30 * a * x + 16 * x ** 2)
        print('G = {:.5f}'.format(G))
    except ZeroDivisionError:
        print('Ошибка расчёта значения G')

# Блок расчёта и вывода данных F
elif value == 2:
    F = 5 ** (a ** 2 - 5 * a * x + 4 * x ** 2)
    print('F = {:5f}'.format(F))

# Блок расчёта и вывода данных Y
elif value == 3:
    try:
        Y = math.log(-a ** 2 - 7 * a * x + 8 * x ** 2 + 1) / math.log(2)
        print('Y = {:.5f}'.format(Y))
    except ValueError:
        print('Ошибка расчёта значения Y')

# Блок ошибки ввода значения value
else:
    print('Такого значения не существует')